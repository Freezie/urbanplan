<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Water;
use Faker\Generator as Faker;

$factory->define(Water::class, function (Faker $faker) {
    return [
        //
        'water_meter_id'=>$faker->numberBetween($min = 1, $max = 40),
        'water_reading' =>$faker->randomElement(['1.97','34.17','10.28','4.36','3.41','17.53','21.32','12.45','19.08','17.69','34.23','12.31','0.98','11.21','23.21','34.42','17.71','21.53','51.23','18.23','19.43','8.21','4.43','5.32','7.41','8.36']),
        
    ];
});
