<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Water_meter;
use Faker\Generator as Faker;

$factory->define(Water_meter::class, function (Faker $faker) {
    return [
        //
        'meter_number'=> $faker->randomElement(['37186990836','39657373765','37635383522','37646241973','35243987261','36452896532','36740913467','34528754935','36479452987','35435264782']),
        'meter_serial_number'=> $faker->randomElement(['WATER-SS37186990836','WATER-SS39657373765','WATER-SS37635383522','WATER-SS37646241973','WATER-SS35243987261','WATER-SS36452896532','WATER-SS36740913467','WATER-SS34528754935','WATER-SS36479452987','WATER-SS35435264782']),
        'property_id'=>$faker->numberBetween($min = 1, $max = 1000),
        'meter_type'=>$faker->randomElement(['digital','manual','automatic']),
    ];
});
