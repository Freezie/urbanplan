<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'sname' => $faker->sname,
        'email' => $faker->unique()->safeEmail,
        'phone_number' => $faker->uniquie()->phoneNumber('en_GB'),
        'address_address' => $faker->unique()->safeEmail,
        'address_latitude' => $faker->unique()->safeEmail,
        'address_longitude' => $faker->unique()->safeEmail,
        'description' => $faker->unique()->safeEmail,
        'social_accounts' => $faker->unique()->safeEmail,
        'image' => $faker->image('public/storage/images',400,300, null, false),
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
