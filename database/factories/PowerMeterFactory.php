<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Power_meter;
use Faker\Generator as Faker;

$factory->define(Power_meter::class, function (Faker $faker) {
    return [
        //
        'meter_number'=> $faker->randomElement(['37186990836','39657373765','37635383522','37646241973','35243987261','36452896532','36740913467','34528754935','36479452987','35435264782']),
        'meter_serial_number'=> $faker->randomElement(['SS37186990836','SS39657373765','SS37635383522','SS37646241973','SS35243987261','SS36452896532','SS36740913467','SS34528754935','SS36479452987','SS35435264782']),
        'property_id'=>$faker->numberBetween($min = 1, $max = 1000),
        'meter_type'=>$faker->randomElement(['digital','manual','automatic']),
    ];
});
