<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('water_meters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('houses');
            $table->integer('meter_serial_number')->nullable();
            $table->string('meter_type'); //meter type digital or manual or token.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('water_meters');
    }
}
