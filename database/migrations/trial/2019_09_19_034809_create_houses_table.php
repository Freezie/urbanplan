<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {  ///List of the rental and bought houses plus the agent id
            $table->bigIncrements('id');
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')->references('id')->on('agents');
            $table->string('house_name');  //refrences on the users table with agent ID. 
            $table->char('property_type',6)->default('rent');  //property type appartment or main house.
            $table->string('transaction')->default('rent'); //on sale or rental.
            $table->integer('price');  //house price for rent or for sale.
            $table->float('floor_area'); //house floor area
            $table->float('bedrooms'); //bedrooms
            $table->float('bathrooms'); //bethrooms
            $table->longText('description')->nullable();  //house description
            $table->float('amenities')->nullable(); //life lyft, garage, interne, security.
            $table->string('image')->nullable(); //house image
             //Address of property
            $table->string('address_address')->nullable();
            $table->double('address_latitude')->nullable();
            $table->double('address_longitude')->nullable();
             //End of address    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
