<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('booking_id');
            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('users');
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            //
            // $table->integer('user_id')->unsigned()->index()->nullable();
            // $table->foreign('user_id')->references('id')->on('users');
            // $table->integer('house_id')->unsigned()->index()->nullable();
            // $table->foreign('house_id')->references('id')->on('houses');
            //

            $table->integer('property_id'); //
            $table->integer('status')->default('1'); //booking status. active or inactive            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
