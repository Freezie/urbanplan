<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {  //These are the property agents.
            $table->bigIncrements('id');
            $table->integer('manager_id')->unsigned();
            $table->foreign('manager_id')->references('id')->on('property_managers');
            $table->char('fname', 15); //agent first name
            $table->char('lname', 15);  //agent second name
            $table->integer('phone_number')->unique();  //agent second name
            $table->string('id_number')->unique();  //agent second name
            $table->string('email')->unique();
            $table->string('image')->nullable(); //house image
            $table->string('social_accounts')->nullable(); //facebook, IG, google
            $table->longText('description')->nullable();  //house description


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
