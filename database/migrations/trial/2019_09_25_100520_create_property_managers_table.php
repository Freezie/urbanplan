<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_managers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('manager_name',100);
            $table->string('manager_phone')->unique();
            $table->string('manager_phone_alternative')->unique()->nullable();
            $table->string('manager_email')->unique();
            $table->string('manager_domain')->unique()->nullable();
            $table->string('social_accounts')->nullable(); //facebook, IG, google
            $table->longText('description')->nullable();  //house description
            //manager_address
            $table->string('address_address')->nullable();
            $table->double('address_latitude')->nullable();
            $table->double('address_longitude')->nullable();
            //end of address
            $table->string('image_logo')->nullable();
           
            
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_managers');
    }
}
