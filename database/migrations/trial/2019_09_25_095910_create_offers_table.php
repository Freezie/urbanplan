<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('property_id')->on('houses');
            $table->dateTimeTz('offer_start')->references('property_id')->on('houses');
            $table->dateTimeTz('offer_end')->references('property_id')->on('houses');
            $table->char('offer_status', 10)->default('inactive');  //active or inactive
            $table->integer('price');  //new house price for rent or for sale.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
