<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('sname')->nullable();
            //$table->string('sname');
            $table->string('email')->unique();
            $table->string('phone_number')->default('0708720092')->unique()->nullable();

            //manager_address
            $table->string('address_address')->nullable();
            $table->double('address_latitude')->nullable();
            $table->double('address_longitude')->nullable();
            //end of address

            $table->double('description')->nullable();
            $table->string('social_accounts')->nullable(); //facebook, IG, google
            $table->string('image')->nullable(); //facebook, IG, google
            $table->string('user_type')->default('user')->nullable(); //user, agent, admin

            //$table->string('phone')->unique();   //We should check if the phone and id number are imputed before grantung full system access...
            //Users cannot have an agent phone number...
            //$table->string('id_number')->unique();   //We should check if the phone and id number are imputed before grantung full system access...
            $table->timestamp('email_verified_at')->nullable();  //this comes later on but phone registration confirmation is also valid.
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
