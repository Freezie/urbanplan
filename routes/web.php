<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact-us', function () {
    return view('contact_us.index');
});
Route::get('/properties-comparison', function () {
    return view('properties-comparison.index');
});

Route::get('/properties-map', function () {
    return view('properties-map.index');
});

Route::get('/properties-search', function () {
    return view('properties-search.index');
});
Route::get('/faq', function () {
    return view('faq.index');
});
Route::get('/services', function () {
    return view('services.index');
});
Route::get('/pricing', function () {
    return view('pricing.index');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/house', 'HouseController@index')->name('house');
