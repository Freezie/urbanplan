@extends('layouts.auth')

@section('content')

<div class="col-xl-4 col-lg-4 col-md-5 content-box p-hdn">
                <div class="content-form-box">
                    <h1 class="login-header">Login</h1>
                    <p>Please enter your user name and password to login</p>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">
                            @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <div class="form-check checkbox-theme">
                                <input class="form-check-input" type="checkbox" value="" id="rememberMe"  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="rememberMe">
                                    Keep Me Signed In
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-color btn-md">Login</button>
                        @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                    </form>


                    
                </div>
                <div class="login-footer clearfix">
                    <div class="pull-left">
                        <a href="index.html"><img src="assets/img/logos/black-logo.png" alt="logo"></a>
                    </div>
                    <div class="pull-right">
                        <p>Don't have an account?<a href="{{ route('register') }}"> Sign Up Now</a></p>
                    </div>
                </div>
            </div>
@stop