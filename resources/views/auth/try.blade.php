@extends('layouts.mainlayout')
@section('content')
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Add User</h2>
        <!-- Page title -->
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        
        <div class="row">
            <div class="col-md-12">
                <!-- Date & Time -->
                <div class="card">
                    <div class="card-header">Add User</div>
                    <div class="card-body">
                        <!-- <form method="POST" action="{{ route('items.store') }}"> -->
                        <form method="POST" action="{{ route('account.store') }}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="name" class="col-form-label">NAME</label>
                                    <input class="form-control" required name="name"  placeholder="USERNAME"  id="name" type="text">
                                        
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="username" class="col-form-label">USERNAME</label>
                                    <input class="form-control @error('username') is-invalid @enderror" required name="username" value="{{ old('name') }}" autocomplete="name" placeholder="USERNAME" id="username" type="text">
                                    @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="role" class="col-form-label">ROLE</label>
                                    <!-- <input class="form-control validate" required name="role" placeholder="ROLE" id="role" type="text"> -->
                                    <select class="browser-default custom-select form-control validate" name="role">
                                        <option selected>Please Select Role</option>
                                        <option value="user">Normal User</option>
                                        <option value="certify">Certify</option>
                                        <option value="admin">Admin</option>
                                        <option value="sadmin">Sys Admin</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="authnumber" class="col-form-label">AUTHORITY NUMBER</label>
                                    <input class="form-control" name="authnumber" placeholder="AUTHORITY NUMBER" id="authnumber" type="text">
                                </div>
                            </div>
                            
                            
                            <button type="submit" class="btn btn-danger">
                            {{ __('Add User') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</main>
@stop