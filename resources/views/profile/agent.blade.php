@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in As Agent

                    <!-- <div class="row">
    @foreach($profile_details as $post)
     <div class='col-md-8'>
       <div class="post"></div>
         {{$post->name}}<br>
         <a href="#" class="btn btn-primary">read it</a>`<br>
       </div>
     @endforeach
    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
