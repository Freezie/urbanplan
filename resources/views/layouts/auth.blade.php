<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Oct 2019 05:05:24 GMT -->
<head>
    @include('shared.links')

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Login page start -->
<div class="login-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-7 overview-bgi cnt-bg-photo cnt-bg-photo-2 d-none d-xl-block d-lg-block d-md-block" style="background-image: url(assets/img/bg-photo-2.jpg)">
                <div class="login-info">
                    <h3>We make spectacular</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
            </div>

            @yield('content')


        </div>
    </div>
</div>
<!-- Login page end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="button" class="btn btn-sm btn-color">Search</button>
    </form>
</div>

@include('shared.scripts')
</body>

<!-- Mirrored from storage.googleapis.com/themevessel-products/xero/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Oct 2019 05:05:24 GMT -->
</html>