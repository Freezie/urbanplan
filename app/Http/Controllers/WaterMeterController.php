<?php

namespace App\Http\Controllers;

use App\Water_meter;
use Illuminate\Http\Request;

class WaterMeterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Water_meter  $water_meter
     * @return \Illuminate\Http\Response
     */
    public function show(Water_meter $water_meter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Water_meter  $water_meter
     * @return \Illuminate\Http\Response
     */
    public function edit(Water_meter $water_meter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Water_meter  $water_meter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Water_meter $water_meter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Water_meter  $water_meter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Water_meter $water_meter)
    {
        //
    }
}
