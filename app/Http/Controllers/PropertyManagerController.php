<?php

namespace App\Http\Controllers;

use App\Property_manager;
use Illuminate\Http\Request;

class PropertyManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property_manager  $property_manager
     * @return \Illuminate\Http\Response
     */
    public function show(Property_manager $property_manager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Property_manager  $property_manager
     * @return \Illuminate\Http\Response
     */
    public function edit(Property_manager $property_manager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Property_manager  $property_manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property_manager $property_manager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property_manager  $property_manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property_manager $property_manager)
    {
        //
    }
}
